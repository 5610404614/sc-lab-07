package controller;

import java.util.ArrayList;

import model.DataSeatPrice;
import model.Seats;
import view.SeatCheckbox;
import view.TheaterSystemGUI;

public class TheaterManagement {
	private ArrayList<SeatCheckbox> chooseSeat = new ArrayList<SeatCheckbox>();
	private DataSeatPrice dataSeatPrice = new DataSeatPrice();
	private Seats seats = new Seats();
	private TheaterSystemGUI gui;

	public TheaterManagement(TheaterSystemGUI gui) {
		this.gui = gui;

	}

	public double getPrice() {
		double totalPrice = 0.0;
		for (int i = 0; i < chooseSeat.size(); i++)
			totalPrice += dataSeatPrice.getSeatPrice(
					chooseSeat.get(i).getRow(), chooseSeat.get(i).getColumn());
		return totalPrice;
	}

	public void selectPrice(String str) {
		if (str != "All") {
			double doublePrice = Double.parseDouble(str);
			for (int i = 0; i < 15; i++) {
				for (int j = 0; j < 20; j++) {

					if ((dataSeatPrice.getSeatPrice(i, j) == doublePrice)
							&& (seats.getStatusSeat(i, j) == true)) {
						gui.setCheckbox(i, j, true);
					} else {
						gui.setCheckbox(i, j, false);
					}
				}
			}
		} else {
			for (int i = 0; i < 15; i++) {
				for (int j = 0; j < 20; j++) {

					if (seats.getStatusSeat(i, j) == true) {
						gui.setCheckbox(i, j, true);
					}
				}
			}
		}

	}
	
	public void selectSchedule(String str) {
		if(str.equals("12.00")){
			gui.scheduleRunner();
		}else if (str.equals("14.00")){
			gui.scheduleRunner();
		}else if (str.equals("16.00")){
			gui.scheduleRunner();
		}
	}

	public void addToSelectItem(SeatCheckbox seatCheckbox) {
		chooseSeat.add(seatCheckbox);
	}

	public void removeSelectItem(SeatCheckbox seatCheckbox) {
		chooseSeat.remove(seatCheckbox);
	}

	public void addBooking() {
		for (int i = 0; i < chooseSeat.size(); i++) {
			seats.booking(chooseSeat.get(i).getRow(), chooseSeat.get(i)
					.getColumn());
			chooseSeat.get(i).setEnabled(false);
		}
		chooseSeat.clear();
	}
}