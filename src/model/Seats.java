package model;

public class Seats {
	private boolean[][] statusSeats;
	
	public Seats() {
		statusSeats = new boolean[15][20];
		for (int i=0;i<15;i++){
			for (int j=0; j<20;j++){
				statusSeats[i][j]=true;
			}
		}
	}
	
	public void booking(int i ,int j){
		statusSeats[i][j]=false;
	}

	public boolean getStatusSeat(int i, int j) {
		return statusSeats[i][j];
	}
	
}
