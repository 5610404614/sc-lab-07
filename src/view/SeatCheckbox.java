package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import view.TheaterSystemGUI;

public class SeatCheckbox extends JCheckBox {
	private int row;
	private int column;
	private TheaterSystemGUI gui;
	
	public SeatCheckbox(int row,int column,TheaterSystemGUI gui){
		this.row = row;
		this.column = column;
		this.gui = gui;
		
		this.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseSeat();
				gui.changePrice();
			}
		});
	}
	
	public void chooseSeat(){
		if(this.isSelected()){
			gui.addSeat(this);
		}
		else{
			gui.removeSeat(this);
	
		}
	}
	
	public String toString(){
		return "["+row+","+column+"]";
	}
	
	public int getRow(){
		return row;
	}
	public int getColumn(){
		return column;
	}
}