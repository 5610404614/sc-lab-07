package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.TheaterManagement;

public class TheaterSystemGUI {
	private JFrame frame;
	private JPanel panelLeft, schedulePanel, choosePricePanel, resultPanel,
			buttonPanel, rightPanel, seatPanel, rowNumPanel, columnTextPanel,
			checkboxPanel;
	private JComboBox<String> comboBoxSchedule, comboBoxPrice;
	private JLabel scheduleText, price, totalText, screen;
	private JButton paid;
	private TheaterManagement control;
	private SeatCheckbox[][] seats;	

	public TheaterSystemGUI() {
		
		this.control = new TheaterManagement(this);
		this.initialize();

	}

	public void initialize() {
		frame = new JFrame("Theater");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLayout(null);

		panelLeft = new JPanel(new GridLayout(10, 1));

		schedulePanel = new JPanel(new FlowLayout());
		scheduleText = new JLabel("Schedule", JLabel.CENTER);
		comboBoxSchedule = new JComboBox<String>();
		comboBoxSchedule.addItem("Schedule of movies");
		comboBoxSchedule.addItem("12.00");
		comboBoxSchedule.addItem("14.00");
		comboBoxSchedule.addItem("16.00");

		schedulePanel.add(scheduleText);
		schedulePanel.add(comboBoxSchedule);
		panelLeft.add(schedulePanel);
		
		scheduleRunner();
		
		
		choosePricePanel = new JPanel(new FlowLayout());
		price = new JLabel("price : ");
		comboBoxPrice = new JComboBox<String>();
		comboBoxPrice.addItem("All");
		comboBoxPrice.addItem("50");
		comboBoxPrice.addItem("40");
		comboBoxPrice.addItem("30");
		comboBoxPrice.addItem("20");
		comboBoxPrice.addItem("10");
		choosePricePanel.add(price);
		choosePricePanel.add(comboBoxPrice);
		priceRunner();
		
		panelLeft.add(choosePricePanel);

		panelLeft.add(new JPanel());
		panelLeft.add(new JPanel());
		panelLeft.add(new JPanel());
		panelLeft.add(new JPanel());
		panelLeft.add(new JPanel());
		panelLeft.add(new JPanel());

		resultPanel = new JPanel(new FlowLayout());
		totalText = new JLabel("Total price : " + control.getPrice() + " Baht.");
		resultPanel.add(totalText);
		panelLeft.add(resultPanel);

		buttonPanel = new JPanel(new FlowLayout());
		paid = new JButton("buy");
		paid.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				control.addBooking();
				changePrice();
			}
		});
		
		buttonPanel.add(paid);
		panelLeft.add(buttonPanel);

		panelLeft.setBorder(BorderFactory.createLineBorder(Color.black));
		frame.add(panelLeft);
		panelLeft.setBounds(20, 20, 200, 540);

		rightPanel = new JPanel(new BorderLayout());

		screen = new JLabel("Screen", JLabel.CENTER);
		rightPanel.add(screen, BorderLayout.NORTH);

		seatPanel = new JPanel(new BorderLayout());

		columnTextPanel = new JPanel(new GridLayout(1, 21));
		columnTextPanel.add(new JLabel(""));
		for (int i = 1; i <= 20; i++) {
			columnTextPanel.add(new JLabel(i + ""));
		}
		seatPanel.add(columnTextPanel, BorderLayout.NORTH);

		rowNumPanel = new JPanel(new GridLayout(15, 1));
		rowNumPanel.add(new JLabel(" A "));
		rowNumPanel.add(new JLabel(" B "));
		rowNumPanel.add(new JLabel(" C "));
		rowNumPanel.add(new JLabel(" D "));
		rowNumPanel.add(new JLabel(" E "));
		rowNumPanel.add(new JLabel(" F "));
		rowNumPanel.add(new JLabel(" G "));
		rowNumPanel.add(new JLabel(" H "));
		rowNumPanel.add(new JLabel(" I "));
		rowNumPanel.add(new JLabel(" L "));
		rowNumPanel.add(new JLabel(" M "));
		rowNumPanel.add(new JLabel(" N "));
		rowNumPanel.add(new JLabel(" O "));
		rowNumPanel.add(new JLabel(" P "));
		rowNumPanel.add(new JLabel(" Q "));

		seatPanel.add(rowNumPanel, BorderLayout.WEST);
		
		seats = new SeatCheckbox[15][20];
		checkboxPanel = new JPanel(new GridLayout(15, 20));
		checkboxPanel.setBackground(new Color(145, 10, 10));
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 20; j++) {
				if ((((j == 8) || (j == 9)) || ((i == 0) && (j <= 1 || j >= 18))
						|| ((i == 1) && (j <= 0 || j >= 19)))&&(i!=14)) {
					checkboxPanel.add(new JLabel(""));
				} else {
					seats[i][j] = new SeatCheckbox(i, j, this);
					seats[i][j].setBackground(new Color(145, 10, 10));
					checkboxPanel.add(seats[i][j]);
				}

			}

		}
		
		seatPanel.add(checkboxPanel, BorderLayout.CENTER);
		rightPanel.add(seatPanel, BorderLayout.CENTER);

		rightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		rightPanel.setBackground(Color.red);
		frame.add(rightPanel);
		rightPanel.setBounds(235, 20, 540, 540);
		

		frame.setVisible(true);
	}

	public void addSeat(SeatCheckbox seatCheckbox) {
		control.addToSelectItem(seatCheckbox);
	}

	public void removeSeat(SeatCheckbox seatCheckbox) {
		control.removeSelectItem(seatCheckbox);
	}

	public void changePrice() {
		totalText.setText("Total price : " + control.getPrice() + " Baht.");
	}

	public void setCheckbox(int i, int j, boolean bool) {
		if (seats[i][j] != null) {
			seats[i][j].setEnabled(bool);
		}
	}
	
	public void scheduleRunner(){
		comboBoxSchedule.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				control.selectSchedule(comboBoxSchedule.getSelectedItem().toString());				
				
			}
		});
	}
	
	public void priceRunner(){
		comboBoxPrice.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.selectPrice(comboBoxPrice.getSelectedItem().toString());
			}
		});
	}
}
